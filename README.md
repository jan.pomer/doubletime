# DoubleTime

DoubleTime gives you independent representation of time. 
Using double for DateTime gives you clean separation of date and time via the decimal point with 64 bit precision.

1) Simple presentation of DateTime

    `27.11.2019 10:29:18` > `20191127.102918`
    
2) Easy incrementation of any part of DateTime

    ```kotlin
        val doubleTime = 20191127.102918.toDoubleTime()
    
        val nextTwoHours = doubleTime + 0.01 + 1.hour()
        println(nextTwoHours) // 20191127.122918
    
        val nextTwoMonths = doubleTime + 100 + 1.month()
        println(nextTwoHours) // 20200127.102918
    
        val doubleSum = doubleTime + doubleTime
        println(doubleSum) // 40391124.205836
    ```

***
<sup>\* This is a joke project and I do not recommend it for production use.</sup>