package com.janpomer.doubletime

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

/**
 * @author Jan Pomer
 * @date 22/11/2019
 */
internal class HelperTest {
    @Test
    fun `convert INT to DoubleTime year`() {
        assertEquals(10000, 1.year())
        assertEquals(1000000, 100.year())
    }

    @Test
    fun `convert INT to DoubleTime month`() {
        assertEquals(200, 2.month())
        assertEquals(1300, 13.month())
        assertEquals(9900, 99.month())
        assertThrows(IllegalArgumentException::class.java, { 100.month() }, "Shouldn't parse more than 99 months.")
        assertThrows(IllegalArgumentException::class.java, { (-1).month() }, "Shouldn't parse less than 0 months.")
    }

    @Test
    fun `convert INT to DoubleTime day`() {
        assertEquals(2, 2.day())
        assertEquals(13, 13.day())
        assertEquals(99, 99.day())
        assertThrows(IllegalArgumentException::class.java, { 100.day() }, "Shouldn't parse more than 99 days.")
        assertThrows(IllegalArgumentException::class.java, { (-1).day() }, "Shouldn't parse less than 0 days.")
    }

    @Test
    fun `convert INT to DoubleTime hour`() {
        assertEquals(0.01, 1.hour())
        assertEquals(0.23, 23.hour())
        assertEquals(1.0, 24.hour())
        assertEquals(1.02, 26.hour())
    }

    @Test
    fun `convert INT to DoubleTime minute`() {
        assertEquals(0.0001, 1.minute())
        assertEquals(0.0059, 59.minute())
        assertEquals(0.01, 60.minute())
        assertEquals(0.0101, 61.minute())
        assertEquals(2.12, 3600.minute())
    }

    @Test
    fun `convert INT to DoubleTime second`() {
        assertEquals(0.000001, 1.second())
        assertEquals(0.000059, 59.second())
        assertEquals(0.0001, 60.second())
        assertEquals(0.000101, 61.second())
        assertEquals(0.01, 3600.second())
    }

    @Test
    fun `convert LONG to DoubleTime year`() {
        assertEquals(10000L, 1L.year())
        assertEquals(1000000L, 100L.year())
    }

    @Test
    fun `convert LONG to DoubleTime month`() {
        assertEquals(200L, 2L.month())
        assertEquals(1300L, 13L.month())
        assertEquals(9900L, 99L.month())
        assertThrows(IllegalArgumentException::class.java, { 100L.month() }, "Shouldn't parse more than 99 months.")
        assertThrows(IllegalArgumentException::class.java, { (-1L).month() }, "Shouldn't parse less than 0 months.")
    }

    @Test
    fun `convert LONG to DoubleTime day`() {
        assertEquals(2L, 2L.day())
        assertEquals(13L, 13L.day())
        assertEquals(99L, 99L.day())
        assertThrows(IllegalArgumentException::class.java, { 100L.day() }, "Shouldn't parse more than 99 days.")
        assertThrows(IllegalArgumentException::class.java, { (-1L).day() }, "Shouldn't parse less than 0 days.")
    }

    @Test
    fun `convert LONG to DoubleTime hour`() {
        assertEquals(0.01, 1L.hour())
        assertEquals(0.23, 23L.hour())
        assertEquals(1.0, 24L.hour())
        assertEquals(1.02, 26L.hour())
    }

    @Test
    fun `convert LONG to DoubleTime minute`() {
        assertEquals(0.0001, 1L.minute())
        assertEquals(0.0059, 59L.minute())
        assertEquals(0.01, 60L.minute())
        assertEquals(0.0101, 61L.minute())
        assertEquals(2.12, 3600L.minute())
    }

    @Test
    fun `convert LONG to DoubleTime second`() {
        assertEquals(0.000001, 1L.second())
        assertEquals(0.000059, 59L.second())
        assertEquals(0.0001, 60L.second())
        assertEquals(0.000101, 61L.second())
        assertEquals(0.01, 3600L.second())
    }
}