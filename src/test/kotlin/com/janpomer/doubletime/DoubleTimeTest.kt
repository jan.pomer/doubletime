package com.janpomer.doubletime

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import java.time.DateTimeException
import java.time.LocalDateTime

/**
 * @author Jan Pomer
 * @date 25/11/2019
 */
internal class DoubleTimeTest {

    @Test
    fun `empty constructor test`() {
        val dt = DoubleTime().toLocalDateTime()
        val ldt = LocalDateTime.of(0, 1, 1, 0, 0, 0)

        assertEquals(ldt, dt)
    }

    @Test
    fun `double constructor test`() {
        val dt = 1812_06_30.10_15_12.toDoubleTime().toLocalDateTime()
        val ldt = LocalDateTime.of(1812, 6, 30, 10, 15, 12)

        assertEquals(ldt, dt)
    }

    @Test
    fun `invalid double constructor test`() {
        assertThrows(
            DateTimeException::class.java,
            { 11812_02_30.10_15_12.toDoubleTime() },
            "Shouldn't parse this date."
        )
    }

    @Test
    fun `LocalDateTime constructor test`() {
        val dt = LocalDateTime.of(1812, 6, 30, 10, 15, 12).toDoubleTime().toLocalDateTime()
        val ldt = LocalDateTime.of(1812, 6, 30, 10, 15, 12)
        assertEquals(ldt, dt)
    }

    @Test
    fun `invalid LocalDateTime constructor test`() {
        assertThrows(
            DateTimeException::class.java,
            { LocalDateTime.of(1812, 2, 30, 10, 15, 12).toDoubleTime() },
            "Shouldn't parse this date."
        )
    }

    @Test
    fun `plus operator for double test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() + 10101.010101
        val ldt = LocalDateTime.of(1813, 7, 26, 11, 16, 13)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `plus operator for int test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() + 10101
        val ldt = LocalDateTime.of(1813, 7, 26,10, 15, 12)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `plus operator for long test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() + 10101L
        val ldt = LocalDateTime.of(1813, 7, 26,10, 15, 12)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `minus operator for double test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() - 10101.010101
        val ldt = LocalDateTime.of(1811, 5, 24, 9, 14, 11)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `minus operator for int test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() - 10101
        val ldt = LocalDateTime.of(1811, 5, 24, 10, 15, 12)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `minus operator for long test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() - 10101L
        val ldt = LocalDateTime.of(1811, 5, 24, 10, 15, 12)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `plus operator for DoubleTime test`() {
        val dt =
            LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() + LocalDateTime.of(1, 1, 1, 1, 1, 1).toDoubleTime()
        val ldt = LocalDateTime.of(1813, 7, 26, 11, 16, 13)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `minus operator for DoubleTime test`() {
        val dt =
            LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() - LocalDateTime.of(1, 1, 1, 1, 1, 1).toDoubleTime()
        val ldt = LocalDateTime.of(1811, 5, 24, 9, 14, 11)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `plus operator for LocalDateTime test`() {
        val dt =
            LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() + LocalDateTime.of(1, 1, 1, 1, 1, 1)
        val ldt = LocalDateTime.of(1813, 7, 26, 11, 16, 13)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `minus operator for LocalDateTime test`() {
        val dt =
            LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime() - LocalDateTime.of(1, 1, 1, 1, 1, 1)
        val ldt = LocalDateTime.of(1811, 5, 24, 9, 14, 11)
        assertEquals(ldt, dt.toLocalDateTime())
    }

    @Test
    fun `toDouble test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime().toDouble()
        val ldt = 18120625.101512
        assertEquals(ldt, dt)
    }

    @Test
    fun `toString test`() {
        val dt = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime().toString()
        assertEquals("18120625.101512", dt)
    }

    @Test
    fun `equals to DoubleTime test`() {
        val dt1 = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime()
        val dt2 = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime()
        assertTrue(dt1 == dt2)
    }

    @Test
    fun `equals to double test`() {
        val dt1 = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime()
        val dt2 = 18120625.101512
        assertTrue(dt1.equals(dt2))
    }

    @Test
    fun `equals to unhandled object test`() {
        val dt1 = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime()
        assertFalse(dt1.equals("dt2"))
    }

    @Test
    fun `compare to DoubleTime test`() {
        val dt1 = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime()

        assertTrue(dt1 > 18110625.101512.toDoubleTime())
        assertTrue(dt1 < 18130625.101512.toDoubleTime())
        assertTrue(dt1 <= 18130625.101512.toDoubleTime())
        assertTrue(dt1 <= 18130625.101512.toDoubleTime())
        assertTrue(dt1 <= 18120625.101512.toDoubleTime())
        assertTrue(dt1 <= 18120625.101512.toDoubleTime())
    }

    @Test
    fun `compare todouble test`() {
        val dt1 = LocalDateTime.of(1812, 6, 25, 10, 15, 12).toDoubleTime()

        assertTrue(dt1 > 18110625.101512)
        assertTrue(dt1 < 18130625.101512)
        assertTrue(dt1 <= 18130625.101512)
        assertTrue(dt1 <= 18130625.101512)
        assertTrue(dt1 <= 18120625.101512)
        assertTrue(dt1 <= 18120625.101512)
    }
}