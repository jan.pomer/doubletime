package com.janpomer.doubletime

import java.time.LocalDateTime
import kotlin.math.pow
import kotlin.math.roundToInt

class DoubleTime {

    private val localDateTime: LocalDateTime

    constructor() {
        localDateTime = 101.parse().toLocalDateTime()
    }

    constructor(value: Double) {
        localDateTime = value.parse().toLocalDateTime()
    }

    constructor(localDateTime: LocalDateTime) {
        this.localDateTime = localDateTime
    }

    operator fun plus(value: Int): DoubleTime {
        val data = value.parse()

        return localDateTime
            .plusDays(data.day.toLong())
            .plusMonths(data.month.toLong())
            .plusYears(data.year.toLong()).toDoubleTime()
    }

    operator fun plus(value: Long): DoubleTime {
        val data = value.parse()

        return localDateTime
            .plusDays(data.day.toLong())
            .plusMonths(data.month.toLong())
            .plusYears(data.year.toLong()).toDoubleTime()
    }

    operator fun plus(value: Double): DoubleTime {
        val data = value.parse()

        return localDateTime
            .plusSeconds(data.second.toLong())
            .plusMinutes(data.minute.toLong())
            .plusHours(data.hour.toLong())
            .plusDays(data.day.toLong())
            .plusMonths(data.month.toLong())
            .plusYears(data.year.toLong()).toDoubleTime()
    }

    operator fun plus(doubleTime: DoubleTime): DoubleTime =
        this.localDateTime
            .plusSeconds(doubleTime.localDateTime.second.toLong())
            .plusMinutes(doubleTime.localDateTime.minute.toLong())
            .plusHours(doubleTime.localDateTime.hour.toLong())
            .plusDays(doubleTime.localDateTime.dayOfMonth.toLong())
            .plusMonths(doubleTime.localDateTime.monthValue.toLong())
            .plusYears(doubleTime.localDateTime.year.toLong()).toDoubleTime()

    operator fun plus(localDateTime: LocalDateTime): DoubleTime =
        this.localDateTime
            .plusSeconds(localDateTime.second.toLong())
            .plusMinutes(localDateTime.minute.toLong())
            .plusHours(localDateTime.hour.toLong())
            .plusDays(localDateTime.dayOfMonth.toLong())
            .plusMonths(localDateTime.monthValue.toLong())
            .plusYears(localDateTime.year.toLong()).toDoubleTime()

    operator fun minus(value: Double): DoubleTime {
        val data = value.parse()

        return localDateTime
            .minusSeconds(data.second.toLong())
            .minusMinutes(data.minute.toLong())
            .minusHours(data.hour.toLong())
            .minusDays(data.day.toLong())
            .minusMonths(data.month.toLong())
            .minusYears(data.year.toLong()).toDoubleTime()
    }

    operator fun minus(value: Int): DoubleTime {
        val data = value.parse()

        return localDateTime
            .minusDays(data.day.toLong())
            .minusMonths(data.month.toLong())
            .minusYears(data.year.toLong()).toDoubleTime()
    }

    operator fun minus(value: Long): DoubleTime {
        val data = value.parse()

        return localDateTime
            .minusDays(data.day.toLong())
            .minusMonths(data.month.toLong())
            .minusYears(data.year.toLong()).toDoubleTime()
    }

    operator fun minus(doubleTime: DoubleTime): DoubleTime =
        this.localDateTime
            .minusSeconds(doubleTime.localDateTime.second.toLong())
            .minusMinutes(doubleTime.localDateTime.minute.toLong())
            .minusHours(doubleTime.localDateTime.hour.toLong())
            .minusDays(doubleTime.localDateTime.dayOfMonth.toLong())
            .minusMonths(doubleTime.localDateTime.monthValue.toLong())
            .minusYears(doubleTime.localDateTime.year.toLong()).toDoubleTime()

    operator fun minus(localDateTime: LocalDateTime): DoubleTime =
        this.localDateTime
            .minusSeconds(localDateTime.second.toLong())
            .minusMinutes(localDateTime.minute.toLong())
            .minusHours(localDateTime.hour.toLong())
            .minusDays(localDateTime.dayOfMonth.toLong())
            .minusMonths(localDateTime.monthValue.toLong())
            .minusYears(localDateTime.year.toLong()).toDoubleTime()

    fun toDouble(): Double {
        val date =
            localDateTime.year.year() + localDateTime.monthValue.month() + localDateTime.dayOfMonth.day()

        val time = ((localDateTime.hour.hour() + localDateTime.minute.minute()).round(
            4
        ) + localDateTime.second.second()).round(6)

        return date + time
    }

    private fun Double.parse(): DateData {
        val date = this.toInt()
        val time = ((this - date) * 1_000_000).roundToInt()

        val year = date / 10_000
        val month = (date - (year * 10_000)) / 100
        val day = (date - (year * 10_000) - (month * 100))

        val hour = (time / 10_000)
        val minute = ((time - (hour * 10_000)) / 100)
        val second = (time - (hour * 10_000) - (minute * 100))

        return DateData(
            year, month, day, hour, minute, second
        )
    }

    private fun Int.parse(): DateData = this.toLong().parse()

    private fun Long.parse(): DateData {
        val year = this / 10_000
        val month = (this - (year * 10_000)) / 100
        val day = (this - (year * 10_000) - (month * 100))

        return DateData(year.toInt(), month.toInt(), day.toInt())
    }

    private fun DateData.toLocalDateTime(): LocalDateTime = LocalDateTime.of(
        this.year,
        this.month,
        this.day,
        this.hour,
        this.minute,
        this.second
    )

    override fun toString(): String {
        return "${this.toDouble().toBigDecimal()}"
    }

    private class DateData(
        val year: Int,
        val month: Int,
        val day: Int,
        val hour: Int = 0,
        val minute: Int = 0,
        val second: Int = 0
    )

    private fun Double.round(n: Int): Double {
        val pow = 10.0.pow(n)
        return (this * pow).roundToInt() / pow
    }

    operator fun compareTo(other: DoubleTime): Int = this.toDouble().compareTo(other.toDouble())

    operator fun compareTo(other: Double): Int = this.toDouble().compareTo(other)

    override fun equals(other: Any?): Boolean = when (other) {
        is Double -> this.toDouble() == other
        is DoubleTime -> this.toDouble() == other.toDouble()
        else -> false
    }

    fun toLocalDateTime(): LocalDateTime = localDateTime

    override fun hashCode(): Int {
        return localDateTime.hashCode()
    }
}