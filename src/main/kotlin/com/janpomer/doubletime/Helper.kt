package com.janpomer.doubletime

import java.time.LocalDateTime

fun Int.year(): Int = this * Constant.YEAR

fun Int.month(): Int {
    if (this > 99 || this < 0) {
        throw IllegalArgumentException("Cannot parse months; max: 99, min: 0")
    }
    return this * Constant.MONTH
}

fun Int.day(): Int {
    if (this > 99 || this < 0) {
        throw IllegalArgumentException("Cannot parse days; max: 99, min: 0")
    }
    return this * Constant.DAY
}

fun Int.hour(): Double {
    val days = this / 24;
    return days + ((this - (days * 24)) * Constant.HOUR)
}

fun Int.minute(): Double {
    val hours = (this / 60)
    return hours.hour() + ((this - (hours * 60)) * Constant.MINUTE)
}

fun Int.second(): Double {
    val minutes = (this / 60)
    return minutes.minute() + ((this - (minutes * 60)) * Constant.SECOND)
}

fun Long.year(): Long = this * Constant.YEAR

fun Long.month(): Long {
    if (this > 99 || this < 0) {
        throw IllegalArgumentException("Cannot parse months; max: 99, min: 0")
    }
    return this * Constant.MONTH
}

fun Long.day(): Long {
    if (this > 99 || this < 0) {
        throw IllegalArgumentException("Cannot parse days; max: 99, min: 0")
    }
    return this * Constant.DAY
}

fun Long.hour(): Double {
    val days = this / 24;
    return days + ((this - (days * 24)) * Constant.HOUR)
}

fun Long.minute(): Double {
    val hours = (this / 60)
    return hours.hour() + ((this - (hours * 60)) * Constant.MINUTE)
}

fun Long.second(): Double {
    val minutes = (this / 60)
    return minutes.minute() + ((this - (minutes * 60)) * Constant.SECOND)
}

fun Double.toDoubleTime(): DoubleTime = DoubleTime(this)

fun LocalDateTime.toDoubleTime(): DoubleTime = DoubleTime(this)