package com.janpomer.doubletime

object Constant {
    const val YEAR: Int = 10_000
    const val MONTH: Int = 100
    const val DAY: Int = 1

    const val HOUR: Double = 0.01
    const val MINUTE: Double = 0.0001
    const val SECOND: Double = 0.000001
}